from django.conf.urls import include, url

urlpatterns = [
    url(r'^api/', include('screator.scapi.urls')),
    url(r'^web/', include('screator.sccore.urls')),
]
