# -*- coding: utf-8 -*-
from ckeditor.fields import RichTextField
from django.conf import settings
from django.db import models
from django.template import Context
from django.template.loader import get_template
from django.template.response import TemplateResponse
from django.utils import six
from django.utils.translation import ugettext_lazy as _
try:
    from django.core.urlresolvers import reverse
except:
    from django.urls import reverse
from django.contrib.contenttypes.models import ContentType

import os
import json
import urllib
import zipfile
import jsonfield

from screator.settings import SCreatorConf

from . import managers
from . import utils
from . import abstracts


class Look(abstracts.ThumbModelBase):
    static_zip_file = models.FileField(
        upload_to="screator/looks",
        verbose_name=_("Static file zip"),
        help_text=_("The zip/tar file must have the following structure: (css/, js/, images/)"),
        blank=True, null=True
    )
    remote_static_files_base_url = models.URLField(verbose_name=_('remote static static files url'),
                                                   blank=True, null=True)
    config = jsonfield.JSONField(editable=False, blank=True, null=True, verbose_name=_("Config"))

    @property
    def config_dict(self):
        if type(self.config) is str:
            return json.loads(self.config)
        return self.config

    @property
    def start_url(self):
        url = ''
        if self.static_zip_file:
            screator_settings = SCreatorConf()
            url = '%s/%s/' % (screator_settings.LOOKS_URL, self.id)
        elif self.remote_static_files_base_url:
            url = self.remote_static_files_base_url
        return url

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None, unzip=True):

        if self.remote_static_files_base_url:
            url = '%s/%s' % (self.remote_static_files_base_url, 'static/config.json')
            response = urllib.urlopen(url)
            response_data = response.read()
            self.config = json.loads(response_data)

        super(Look, self).save(force_insert=force_insert, force_update=force_update,
                               using=using, update_fields=update_fields)

        if self.static_zip_file and unzip:
            screator_settings = SCreatorConf()

            extract_dir = os.path.join(screator_settings.LOOKS_ROOT, str(self.id))
            try:
                os.makedirs(extract_dir)
            except:
                pass

            zip_file = zipfile.ZipFile(self.static_zip_file.path)
            zip_file.extractall(extract_dir)

            try:
                config_file = open(os.path.join(extract_dir, 'static/config.json'), 'r')
                self.config = config_file.read()
                config_file.close()
                self.save(unzip=False)
            except:
                pass

    class Meta:
        verbose_name = _("Look")
        verbose_name_plural = _("Looks")


class WebSite(abstracts.SlugModelBase):
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name=_("owner"),
        editable=False,
        related_name="owned_sites",
        on_delete=models.PROTECT
    )
    look = models.ForeignKey(
        Look,
        verbose_name=_("Look"),
        on_delete=models.PROTECT
    )
    public = models.BooleanField(verbose_name=_("public"), default=True, editable=False)
    go_public_at = models.DateTimeField(
        verbose_name=_("go public date/time"),
        help_text=_("Please add a date-time in the form YYYY-MM-DD hh:mm."),
        blank=True,
        null=True
    )
    first_published_at = models.DateTimeField(
        verbose_name=_("first published at"),
        null=True,
        editable=False,
        db_index=True
    )

    class Meta:
        verbose_name = _("Web Site")
        verbose_name_plural = _("Web Sites")

    @property
    def menu(self):
        return self.page_set.filter(show_in_menus=True, public=True).order_by('menu_order')

    @property
    def pages(self):
        return self.page_set.all().order_by('menu_order')

    @property
    def url(self):
        screator_settings = SCreatorConf()
        return screator_settings.DOMAIN + reverse('website_detail', args=(self.slug,))


class PageModelBase(models.base.ModelBase):
    """
    Metaclass for all pages.
    """
    def __init__(cls, name, bases, dct):
        super(PageModelBase, cls).__init__(name, bases, dct)

        # Override the default `objects` attribute with a `PageManager`.
        # Managers are not inherited by MTI child models, so `Page` subclasses
        # will get a plain `Manager` instead of a `PageManager`.
        # If the developer has set their own custom `Manager` subclass, do not
        # clobber it.
        if not cls._meta.abstract and type(cls.objects) is models.Manager:
            managers.PageManager().contribute_to_class(cls, "objects")

        if "template_name" not in dct:
            # Define a default template path derived from the app name and model name
            cls.template_name = "%s/%s.html" % (cls._meta.app_label, utils.camelcase_to_underscore(name))


class Page(six.with_metaclass(PageModelBase, models.Model)):
    website = models.ForeignKey(
        WebSite,
        verbose_name=_("website"),
        on_delete=models.CASCADE
    )
    page_type = models.CharField(
        verbose_name=_("page_type"),
        max_length=255,
        editable=False
    )
    title = models.CharField(
        verbose_name=_("title"),
        max_length=255,
        help_text=_("The title as you'd like it to be seen by the public")
    )
    slug = models.SlugField(
        verbose_name=_("slug"),
        max_length=255,
        help_text=_("The title as it will appear in URLs e.g http://domain.com/[my-slug]/")
    )
    seo_title = models.CharField(
        verbose_name=_("seo page title"),
        max_length=255,
        blank=True,
        help_text=_("Optional. 'Search Engine Friendly' title. This will appear at the top of the browser window.")
    )
    seo_keywords = models.CharField(
        verbose_name=_("seo keywords"),
        max_length=255,
        blank=True,
        help_text=_("Optional. 'Search Engine Friendly' keywords.")
    )    
    seo_description = models.CharField(
        verbose_name=_("seo description"),
        max_length=255,
        blank=True,
        help_text=_("Optional. 'Search Engine Friendly' description.")
    )
    content = RichTextField(
        verbose_name=_("content"),
        blank=True,
        null=True
    )
    show_in_menus = models.BooleanField(
        verbose_name=_("show in menus"),
        default=True,
        help_text=_("Whether a link to this page will appear in automatically generated menus")
    )
    menu_order = models.PositiveSmallIntegerField(
        verbose_name=_("order"),
        default=0
    )
    public = models.BooleanField(verbose_name=_("public"), default=True)
    go_public_at = models.DateTimeField(
        verbose_name=_("go public date/time"),
        help_text=_("Please add a date-time in the form YYYY-MM-DD hh:mm."),
        blank=True,
        null=True
    )
    latest_revision_created_at = models.DateTimeField(
        verbose_name=_("latest revision created at"),
        auto_now=True
    )
    created_at = models.DateTimeField(
        verbose_name=_("latest revision created at"),
        auto_now_add=True
    )

    objects = managers.PageManager()

    class Meta:
        verbose_name = _("Page")
        verbose_name_plural = _("Pages")
        ordering = ['menu_order']

    def __str__(self):              # __unicode__ on Python 2
        return "%s" % self.title

    def save(self, *args, **kwargs):
        ctype = ContentType.objects.get_for_model(self)
        self.page_type = '%s.%s' % (ctype.app_label, ctype.model)
        super(Page, self).save(*args, **kwargs)

    def unpublish(self, commit=True):
        if self.public:
            self.public = False

            if commit:
                self.save()
            # logger.info("Page unpublished: \"%s\" id=%d", self.title, self.id)

    def get_model(self):
        return utils.resolve_model_string(self.page_type, self._meta.app_label)

    def get_object(self):
        return self.get_model().objects.get(pk=self.pk)

    def get_context(self, request, *args, **kwargs):
        return {
            "content": self,
            "object": self.get_object(),
            "request": request,
            "website": self.website
        }

    def get_template_name(self):
        return self.template_name

    def get_template(self):
        return get_template(self.get_template_name())

    def render(self, request=None):
        template = self.get_template()
        context_dict = self.get_context(request)
        try:
            context = Context(self.get_context(request))
            return template.render(context)
        except TypeError:
            return template.render(context_dict)

    def serve(self, request, *args, **kwargs):
        return TemplateResponse(
            request,
            self.get_template_name(),
            self.get_context(request, *args, **kwargs)
        )


"""
 *
 * Content model to handler different types of contents like gallery, contact form, etc...
 *
"""


class SimplePage(Page):
    class Meta:
        verbose_name = _("Simple page")
        verbose_name_plural = _("Simple pages")


class VideoPage(Page):
    video_url = models.URLField(verbose_name=_("video url"))

    class Meta:
        verbose_name = _("Video page")
        verbose_name_plural = _("Video pages")


class MapPage(Page):
    address = models.CharField(
        verbose_name=_("address"),
        max_length=255
    )

    class Meta:
        verbose_name = _("Map page")
        verbose_name_plural = _("Map pages")


class GalleryPage(Page):
    class Meta:
        verbose_name = _("Gallery page")
        verbose_name_plural = _("Gallery pages")


class GalleryImage(models.Model):
    title = models.CharField(
        verbose_name=_("title"),
        max_length=255,
        blank=True,
        null=True
    )
    description = models.CharField(
        verbose_name=_("description"),
        max_length=255,
        blank=True,
        null=True
    )
    image = models.ImageField(
        verbose_name=_("image"),
        upload_to="screator/gallery_images"
    )
    order = models.PositiveSmallIntegerField(
        verbose_name=_("order"),
        default=0
    )

    gallery_page = models.ForeignKey(
        GalleryPage,
        verbose_name=_("Gallery page"),
        on_delete=models.CASCADE
    )

    class Meta:
        verbose_name = _("Gallery image")
        verbose_name_plural = _("Gallery images")
        ordering = ['order']


"""
class ContactPage(models.Model):
    name = models.CharField(
        verbose_name=_("name"),
        max_length=255
    )
    subject = models.CharField(
        verbose_name=_("subject"),
        max_length=255
    )
    email = models.EmailField(verbose_name=_("email"))
    content = RichTextField(verbose_name=_("content"))

    class Meta:
        verbose_name = _("Contact page")
        verbose_name_plural = _("Contact pages")
"""
