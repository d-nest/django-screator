from django.views.generic import DetailView
from django.shortcuts import get_object_or_404, redirect
try:
    from django.core.urlresolvers import reverse
except:
    from django.urls import reverse
from django.http.response import Http404

from . import models


class WebSiteDetailView(DetailView):
    model = models.WebSite

    def get(self, request, *args, **kwargs):
        obj = self.get_object()
        try:
            home = obj.menu[0]
        except:
            return Http404()
        return redirect(reverse('page_detail', args=(obj.slug, home.slug,)))


class PageDetailView(DetailView):
    model = models.Page

    def get_object(self, queryset=None):
        obj = get_object_or_404(models.Page, website__slug=self.kwargs['website_slug'], slug=self.kwargs['slug'])
        return obj.get_object()

    def get_template_names(self):
        """
        Returns a list of template names to be used for the request. Must return
        a list. May not be called if render_to_response is overridden.
        """
        obj = self.get_object()
        return [obj.get_template_name()]

    def get(self, request, *args, **kwargs):
        obj = self.get_object()
        context = obj.get_context(request, **kwargs)
        return self.render_to_response(context)
