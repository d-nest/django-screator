# -*- coding: utf-8 -*-
from django import template

try:
    from urllib.parse import urlparse
except ImportError:
    from urlparse import urlparse

register = template.Library()


@register.filter(name='absolute')
def absolute(value, start=''):
    if bool(urlparse(value).netloc):
        return value
    return start + value

