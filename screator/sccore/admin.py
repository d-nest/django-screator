# -*- coding: utf-8 -*-
from django.contrib import admin

from . import models

admin.site.register(models.Look)


class WebSiteAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("title",)}
    list_display = ["title", "slug", "look"]

    def save_model(self, request, obj, form, change):
        if not obj.owner_id:
            obj.owner = request.user
        super(WebSiteAdmin, self).save_model(request, obj, form, change)

admin.site.register(models.WebSite, WebSiteAdmin)


class SlugBaseModelAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("title",)}


class PageAdmin(SlugBaseModelAdmin):
    list_display = ["title", "page_type"]

admin.site.register(models.Page, PageAdmin)

admin.site.register(models.SimplePage, SlugBaseModelAdmin)
admin.site.register(models.MapPage, SlugBaseModelAdmin)
admin.site.register(models.VideoPage, SlugBaseModelAdmin)

admin.site.register(models.GalleryImage)


class GalleryImageInline(admin.TabularInline):
    model = models.GalleryImage


class GalleryPageAdmin(SlugBaseModelAdmin):
    inlines = [GalleryImageInline, ]

admin.site.register(models.GalleryPage, GalleryPageAdmin)
