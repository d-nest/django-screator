# -*- coding: utf-8 -*-
from django.db import models
from . import query


class BasePageManager(models.Manager):
    def get_queryset(self):
        return query.PageQuerySet(self.model).order_by('created_at')

PageManager = BasePageManager.from_queryset(query.PageQuerySet)
