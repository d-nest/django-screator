from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^(?P<slug>[-\w]+)/$',
        views.WebSiteDetailView.as_view(), name='website_detail'),
    url(r'^(?P<website_slug>[-\w]+)/(?P<slug>[-\w]+)/$',
        views.PageDetailView.as_view(), name='page_detail'),
]
