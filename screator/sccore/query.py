from django.db.models import Q
from django.db.models.query import QuerySet


class PageQuerySet(QuerySet):
    def public_q(self):
        return Q(public=True)

    def public(self):
        """
        This filters the QuerySet to only contain published pages.
        """
        return self.filter(self.public_q())

    def not_public(self):
        """
        This filters the QuerySet to only contain unpublished pages.
        """
        return self.exclude(self.public_q())

    def in_menu_q(self):
        return Q(show_in_menus=True)

    def in_menu(self):
        """
        This filters the QuerySet to only contain pages that are in the menus.
        """
        return self.filter(self.in_menu_q())

    def not_in_menu(self):
        """
        This filters the QuerySet to only contain pages that are not in the menus.
        """
        return self.exclude(self.in_menu_q())

    def page_q(self, other):
        return Q(id=other.id)

    def page(self, other):
        """
        This filters the QuerySet so it only contains the specified page.
        :param other; Other page
        """
        return self.filter(self.page_q(other))

    def not_page(self, other):
        """
        This filters the QuerySet so it doesn't contain the specified page.
        :param other; Other page
        """
        return self.exclude(self.page_q(other))

    def unpublish(self):
        """
        This unpublishes all live pages in the QuerySet.
        """
        for page in self.public():
            page.unpublish()
