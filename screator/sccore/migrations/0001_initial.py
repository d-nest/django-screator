# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings
import django.db.models.deletion
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='GalleryImage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='title')),
                ('description', models.CharField(max_length=255, verbose_name='description')),
                ('image', models.ImageField(upload_to=b'screator/gallery_images', verbose_name='image')),
            ],
            options={
                'verbose_name': 'Gallery image',
                'verbose_name_plural': 'Gallery images',
            },
        ),
        migrations.CreateModel(
            name='Look',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(help_text="The title as you'd like it to be seen by the public", max_length=255, verbose_name='title')),
                ('thumbnail', models.ImageField(upload_to=b'screator/thumbnails', verbose_name='Thumbnail')),
                ('static_zip_file', models.FileField(help_text='The zip/tar file must have the following structure: (css/, js/, images/)', upload_to=b'screator/looks', verbose_name='Static file zip')),
            ],
            options={
                'verbose_name': 'Look',
                'verbose_name_plural': 'Looks',
            },
        ),
        migrations.CreateModel(
            name='Page',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('page_type', models.CharField(verbose_name='page_type', max_length=255, editable=False)),
                ('title', models.CharField(help_text="The title as you'd like it to be seen by the public", max_length=255, verbose_name='title')),
                ('slug', models.SlugField(help_text='The title as it will appear in URLs e.g http://domain.com/[my-slug]/', max_length=255, verbose_name='slug')),
                ('seo_title', models.CharField(help_text="Optional. 'Search Engine Friendly' title. This will appear at the top of the browser window.", max_length=255, verbose_name='seo page title', blank=True)),
                ('content', ckeditor.fields.RichTextField(null=True, verbose_name='content', blank=True)),
                ('show_in_menus', models.BooleanField(default=True, help_text='Whether a link to this page will appear in automatically generated menus', verbose_name='show in menus')),
                ('public', models.BooleanField(default=True, verbose_name='public')),
                ('go_public_at', models.DateTimeField(help_text='Please add a date-time in the form YYYY-MM-DD hh:mm.', null=True, verbose_name='go public date/time', blank=True)),
                ('latest_revision_created_at', models.DateTimeField(auto_now=True, verbose_name='latest revision created at')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='latest revision created at')),
            ],
            options={
                'verbose_name': 'Page',
                'verbose_name_plural': 'Pages',
            },
        ),
        migrations.CreateModel(
            name='WebSite',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(help_text="The title as you'd like it to be seen by the public", max_length=255, verbose_name='title')),
                ('public', models.BooleanField(default=True, verbose_name='public', editable=False)),
                ('go_public_at', models.DateTimeField(help_text='Please add a date-time in the form YYYY-MM-DD hh:mm.', null=True, verbose_name='go public date/time', blank=True)),
                ('first_published_at', models.DateTimeField(verbose_name='first published at', null=True, editable=False, db_index=True)),
                ('look', models.ForeignKey(verbose_name='Look', to='sccore.Look', on_delete=django.db.models.deletion.PROTECT)),
                ('owner', models.ForeignKey(related_name='owned_sites', editable=False, to=settings.AUTH_USER_MODEL, verbose_name='owner', on_delete=django.db.models.deletion.PROTECT)),
            ],
            options={
                'verbose_name': 'Web Site',
                'verbose_name_plural': 'Web Sites',
            },
        ),
        migrations.CreateModel(
            name='GalleryPage',
            fields=[
                ('page_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='sccore.Page', on_delete=django.db.models.deletion.CASCADE)),
            ],
            options={
                'verbose_name': 'Gallery page',
                'verbose_name_plural': 'Gallery pages',
            },
            bases=('sccore.page',),
        ),
        migrations.CreateModel(
            name='MapPage',
            fields=[
                ('page_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='sccore.Page', on_delete=django.db.models.deletion.CASCADE)),
                ('address', models.CharField(max_length=255, verbose_name='address')),
            ],
            options={
                'verbose_name': 'Map page',
                'verbose_name_plural': 'Map pages',
            },
            bases=('sccore.page',),
        ),
        migrations.CreateModel(
            name='SimplePage',
            fields=[
                ('page_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='sccore.Page', on_delete=django.db.models.deletion.CASCADE)),
            ],
            options={
                'verbose_name': 'Simple page',
                'verbose_name_plural': 'Simple pages',
            },
            bases=('sccore.page',),
        ),
        migrations.CreateModel(
            name='VideoPage',
            fields=[
                ('page_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='sccore.Page', on_delete=django.db.models.deletion.CASCADE)),
                ('video_url', models.URLField(verbose_name='video url')),
            ],
            options={
                'verbose_name': 'Video page',
                'verbose_name_plural': 'Video pages',
            },
            bases=('sccore.page',),
        ),
        migrations.AddField(
            model_name='page',
            name='website',
            field=models.ForeignKey(verbose_name='website', to='sccore.WebSite', on_delete=django.db.models.deletion.CASCADE),
        ),
        migrations.AddField(
            model_name='galleryimage',
            name='gallery_page',
            field=models.ForeignKey(verbose_name='Gallery page', to='sccore.GalleryPage', on_delete=django.db.models.deletion.CASCADE),
        ),
    ]
