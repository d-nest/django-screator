# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('sccore', '0003_auto_20160421_1723'),
    ]

    operations = [
        migrations.AddField(
            model_name='look',
            name='config',
            field=jsonfield.fields.JSONField(verbose_name='Config', null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='galleryimage',
            name='image',
            field=models.ImageField(verbose_name='image', upload_to='screator/gallery_images'),
        ),
        migrations.AlterField(
            model_name='look',
            name='static_zip_file',
            field=models.FileField(verbose_name='Static file zip', upload_to='screator/looks', help_text='The zip/tar file must have the following structure: (css/, js/, images/)'),
        ),
        migrations.AlterField(
            model_name='look',
            name='thumbnail',
            field=models.ImageField(verbose_name='Thumbnail', upload_to='screator/thumbnails'),
        ),
    ]
