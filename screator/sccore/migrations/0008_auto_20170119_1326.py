# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sccore', '0007_auto_20161021_1049'),
    ]

    operations = [
        migrations.AddField(
            model_name='look',
            name='remote_static_files_base_url',
            field=models.URLField(null=True, verbose_name='remote static static files url', blank=True),
        ),
        migrations.AlterField(
            model_name='look',
            name='static_zip_file',
            field=models.FileField(help_text='The zip/tar file must have the following structure: (css/, js/, images/)', upload_to=b'screator/looks', null=True, verbose_name='Static file zip', blank=True),
        ),
    ]
