# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sccore', '0002_auto_20160421_1722'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='page',
            options={'ordering': ['menu_order'], 'verbose_name': 'Page', 'verbose_name_plural': 'Pages'},
        ),
        migrations.AlterField(
            model_name='page',
            name='menu_order',
            field=models.PositiveSmallIntegerField(default=0, verbose_name='order'),
        ),
    ]
