# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sccore', '0005_auto_20161018_1147'),
    ]

    operations = [
        migrations.AddField(
            model_name='website',
            name='slug',
            field=models.SlugField(verbose_name='slug', max_length=255, default=' ', help_text='The title as it will appear in URLs e.g http://domain.com/[my-slug]/'),
            preserve_default=False,
        ),
    ]
