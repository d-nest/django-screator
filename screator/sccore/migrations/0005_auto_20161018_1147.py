# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('sccore', '0004_auto_20161018_1141'),
    ]

    operations = [
        migrations.AlterField(
            model_name='look',
            name='config',
            field=jsonfield.fields.JSONField(blank=True, null=True, editable=False, verbose_name='Config'),
        ),
    ]
