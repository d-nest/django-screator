# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sccore', '0006_website_slug'),
    ]

    operations = [
        migrations.AlterField(
            model_name='galleryimage',
            name='description',
            field=models.CharField(null=True, blank=True, max_length=255, verbose_name='description'),
        ),
        migrations.AlterField(
            model_name='galleryimage',
            name='title',
            field=models.CharField(null=True, blank=True, max_length=255, verbose_name='title'),
        ),
    ]