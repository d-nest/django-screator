# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sccore', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='galleryimage',
            options={'ordering': ['order'], 'verbose_name': 'Gallery image', 'verbose_name_plural': 'Gallery images'},
        ),
        migrations.AddField(
            model_name='galleryimage',
            name='order',
            field=models.PositiveSmallIntegerField(default=0, verbose_name='order'),
        ),
        migrations.AddField(
            model_name='page',
            name='menu_order',
            field=models.PositiveSmallIntegerField(null=True, verbose_name='order', blank=True),
        ),
    ]
