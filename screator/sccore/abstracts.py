# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _


class TitleModelBase(models.Model):
    title = models.CharField(
        verbose_name=_("title"),
        max_length=255,
        help_text=_("The title as you'd like it to be seen by the public")
    )

    class Meta:
        abstract = True

    def __str__(self):
        return self.title


class SlugModelBase(TitleModelBase):
    slug = models.SlugField(
        verbose_name=_("slug"),
        max_length=255,
        help_text=_("The title as it will appear in URLs e.g http://domain.com/[my-slug]/")
    )

    class Meta:
        abstract = True


class ThumbModelBase(TitleModelBase):
    thumbnail = models.ImageField(upload_to='screator/thumbnails', verbose_name=_('Thumbnail'))

    class Meta:
        abstract = True
