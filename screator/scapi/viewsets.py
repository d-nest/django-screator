from django.utils.text import slugify

from rest_framework import viewsets, exceptions
from rest_framework import filters, status
from rest_framework.generics import get_object_or_404
from django_filters.rest_framework import DjangoFilterBackend

from screator.sccore import models
from . import serializers


class LookViewSet(viewsets.ModelViewSet):
    queryset = models.Look.objects.all()
    serializer_class = serializers.LookSerializer
    filter_backends = (filters.OrderingFilter, )
    ordering_fields = ('title', )


class WebSiteViewSet(viewsets.ModelViewSet):
    queryset = models.WebSite.objects.all()
    filter_backends = (filters.OrderingFilter, )
    ordering_fields = ('title', )

    def get_serializer_class(self):
        show_detail_for = ['list', 'retrieve']
        if self.action in show_detail_for:
            return serializers.WebSiteRetrieveSerializer
        return serializers.WebSiteSerializer

    def perform_create(self, serializer):
        serializer.validated_data['owner'] = self.request.user
        serializer.save()

    def get_serializer_context(self):
        return {'request': self.request}


class GalleryImagesViewSet(viewsets.ModelViewSet):
    queryset = models.GalleryImage.objects.all()
    serializer_class = serializers.GalleryImageSerializer
    filter_backends = (filters.OrderingFilter, )
    ordering_fields = ('title', )

    def perform_create(self, serializer):
        gallery_page = models.GalleryPage.objects.get(pk=self.kwargs.get('gallery_page_pk'))
        serializer.validated_data['gallery_page'] = gallery_page
        serializer.save()


class PageBaseViewSet(viewsets.ModelViewSet):
    filter_backends = (filters.OrderingFilter, DjangoFilterBackend)
    ordering_fields = ('title', )
    filter_fields = ('show_in_menus',)

    def perform_create(self, serializer):
        website = models.WebSite.objects.get(pk=self.kwargs.get('website_pk'))
        serializer.validated_data['website'] = website
        serializer.validated_data['slug'] = slugify(serializer.validated_data['title'])
        serializer.save()

    def get_serializer_context(self):
        return {'request': self.request}

    def get_queryset(self):
        return self.queryset.filter(website__id=self.kwargs.get('website_pk'))

    def get_object(self):
        try:
            obj = super(PageBaseViewSet, self).get_object()
        except:
            queryset = self.filter_queryset(self.get_queryset())
            lookup_url_kwarg = self.lookup_url_kwarg or self.lookup_field
            filter_kwargs = {'slug': self.kwargs[lookup_url_kwarg]}
            obj = get_object_or_404(queryset, **filter_kwargs)
            
            # May raise a permission denied
            self.check_object_permissions(self.request, obj)
        return obj


class GalleryPageViewSet(PageBaseViewSet):
    queryset = models.GalleryPage.objects.all()
    serializer_class = serializers.GalleryPageSerializer


class SimplePageViewSet(PageBaseViewSet):
    queryset = models.SimplePage.objects.all()
    serializer_class = serializers.SimplePageSerializer


class VideoPageViewSet(PageBaseViewSet):
    queryset = models.VideoPage.objects.all()
    serializer_class = serializers.VideoPageSerializer


class MapPageViewSet(PageBaseViewSet):
    queryset = models.MapPage.objects.all()
    serializer_class = serializers.MapPageSerializer
