from django.contrib.auth import get_user_model

from rest_framework import serializers

from screator.sccore import models


class PageSummarySerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Page
        fields = ('pk', 'title')
        read_only_fields = fields


class PageSerializer(serializers.ModelSerializer):
    rendered_content = serializers.SerializerMethodField()

    class Meta:
        model = models.Page
        read_only_fields = ('slug', )
        exclude = ('website', )

    def get_rendered_content(self, obj):
        request = self.context.get('request', None)
        page_obj = obj.get_object()
        return page_obj.render(request)


class SimplePageSerializer(PageSerializer):
    class Meta(PageSerializer.Meta):
        model = models.SimplePage


class VideoPageSerializer(PageSerializer):
    class Meta(PageSerializer.Meta):
        model = models.VideoPage


class MapPageSerializer(PageSerializer):
    class Meta(PageSerializer.Meta):
        model = models.MapPage


class GalleryImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.GalleryImage
        exclude = ('gallery_page', )


class GalleryPageSerializer(PageSerializer):
    images = GalleryImageSerializer(many=True, source='galleryimage_set', read_only=True)

    class Meta(PageSerializer.Meta):
        model = models.GalleryPage


class OwnerSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField()

    class Meta:
        model = get_user_model()
        fields = ('email', 'name')

    def get_name(self, obj):
        return obj.get_full_name()


class LookSerializer(serializers.ModelSerializer):
    start_url = serializers.SerializerMethodField()

    @staticmethod
    def get_start_url(obj):
        return obj.start_url

    class Meta:
        model = models.Look
        exclude = ('config',)
        extra_kwargs = {
            'static_zip_file': {'write_only': True},
            'remote_static_files_base_url': {'write_only': True}
        }


class WebSiteRetrieveSerializer(serializers.ModelSerializer):
    page_set = PageSerializer(many=True, source='pages', read_only=True)
    owner = OwnerSerializer()
    look = LookSerializer()
    url = serializers.CharField(read_only=True)

    class Meta:
        fields = '__all__'
        model = models.WebSite


class WebSiteSerializer(serializers.ModelSerializer):
    url = serializers.CharField(read_only=True)

    class Meta:
        fields = '__all__'
        model = models.WebSite
