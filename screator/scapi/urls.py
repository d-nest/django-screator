from django.conf.urls import url, include

from rest_framework.routers import DefaultRouter
from rest_framework_nested.routers import NestedSimpleRouter

from . import viewsets

router = DefaultRouter()
router.register(r'looks', viewsets.LookViewSet)
router.register(r'websites', viewsets.WebSiteViewSet)

website_router = NestedSimpleRouter(router, r'websites', lookup='website')
website_router.register(r'map-pages', viewsets.MapPageViewSet, base_name='map_pages')
website_router.register(r'video-pages', viewsets.VideoPageViewSet, base_name='video_pages')
website_router.register(r'simple-pages', viewsets.SimplePageViewSet, base_name='simple_pages')
website_router.register(r'gallery-pages', viewsets.GalleryPageViewSet, base_name='gallery_pages')

gallery_router = NestedSimpleRouter(website_router, r'gallery-pages', lookup='gallery_page')
gallery_router.register(r'images', viewsets.GalleryImagesViewSet, base_name='gallery_images')

urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^', include(website_router.urls)),
    url(r'^', include(gallery_router.urls)),
]
