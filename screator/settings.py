# -*- coding: utf-8 -*-
from appconf import AppConf

class SCreatorConf(AppConf):
     LOOKS_ROOT = 'looks/'
     LOOKS_URL = 'looks/'
     DOMAIN = ''

     class Meta:
         proxy = True

