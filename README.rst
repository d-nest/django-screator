========
SCreator
========

This package contains SCreator tools.

License
-------

The package is released under the New BSD license.

Example of use
--------------

To install this app and use, you must do the following steps:

1. Add the following apps to INSTALLED_APPS:

    .. code-block:: python

        INSTALLED_APPS = (
            ...
            'ckeditor',
            'rest_framework',
            'embed_video',
            'easy_maps',

            'screator.scapi',
            'screator.sccore',
            ...
        )


2. Set the following properties in settings:

    .. code-block:: python

        SCREATOR_LOOKS_ROOT -> Represent the absolute path to save the static files
        SCREATOR_LOOKS_URL -> Represent the path on url to get the static files
        SCREATOR_DOMAIN -> Represent the domain where the sites are hosted


3. Add urls:

    .. code-block:: python

        url(r'^', include(screator.urls)),

        # OR ...

        url(r'^', include(screator.scapi.urls)),
        url(r'^', include(screator.sccore.urls)),
