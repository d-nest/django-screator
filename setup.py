# -*- coding: utf-8 -*-
from setuptools import setup, find_packages

# Dynamically calculate the version based on dcatastro.VERSION.
VERSION = __import__('screator').get_version()


setup(
    name='screator',
    version=VERSION,
    url='https://bitbucket.org/abalt/screator',
    author='DNest',
    author_email='admin@dnesteagency.com',
    description=(
        "SCreator Tools"),
    long_description=open('README.rst').read(),
    keywords="SCreator site-creator styles",
    license=open('LICENSE').read(),
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        'django>=1.8.9',
        'django-ckeditor>=5.0.3',
        'djangorestframework>=3.3.2',
        'django-filter>=0.13.0',
        'drf-nested-routers>=0.11.1',
        'pillow>=3.4.1',
        'django-appconf>=1.0.2',
        'jsonfield>=1.0.3',
        'django-easy-maps>=0.9.2',
        'django-embed-video>=1.1.0'
    ],
    download_url='https://bitbucket.org/abalt/screator/get/0.1.14.zip',
    # See http://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Topic :: Other/Nonlisted Topic'],
)
